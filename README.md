# Tree-sitter hello world

Using this as a playground for experimenting with Tree-sitter's Python bindings to parse Go code.

## Setup

- Python 3.8

```shell
# Pull the Go grammar
$ git submodule update --init

# Set up the Tree-sitter Python bindings
$ python3 -m venv ./venv
$ source ./venv/bin/activate
$ pip3 install tree_sitter

# Execute the playground
$ ./hello_world
```

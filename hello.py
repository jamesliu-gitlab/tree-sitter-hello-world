#!/usr/bin/env python3

import base64
from tree_sitter import Language, Parser

Language.build_library(

    'build/my-languages.so',
    [
        'vendor/tree-sitter-go'
    ]
)

GO_LANGUAGE = Language('build/my-languages.so', 'go')

SOURCE = """
package main

import "fmt"

func add(int a, b) int {
    return a + b
}

func main() {
    a := 42
	fmt.Println("Hello, World!")
	fmt.Println(add(a, 2))
}
"""


# (source_file
#   (package_clause
#     (package_identifier))
#   (import_declaration
#     (import_spec
#       path: (interpreted_string_literal)))
#   (function_declaration
#     name: (identifier)
#     parameters: (parameter_list)
#     body: (block
#             (call_expression
#               function: (selector_expression
#                           operand: (identifier)
#                           field: (field_identifier))
#               arguments: (argument_list
#                            (interpreted_string_literal))))))


def id(node):
    return base64.b64encode(bytes(f"{node.start_point}{node.end_point}{node.text}", 'utf-8')).decode('utf-8').strip('=')


def print_node(node, indent):
    print(f"{' ' * indent}{id(node)}[{node.type}] :: {node.start_point} :: {node.text}")


def mermaid_statement(node, prev_node):
    return f"{id(prev_node)}(({prev_node.type}))-->{id(node)}(({node.type}))"


def prev_named_node(node):
    node = node.parent
    while node is not None:
        if node.is_named:
            break
        node = node.goto_parent()
    return node


def walk(tree):
    cursor = tree.walk()
    done = False
    recurse = True
    indent = -2
    prev_node = cursor.node

    mermaid = []

    while not done:
        if recurse and cursor.goto_first_child():
            indent += 2
            recurse = True
            if cursor.node.is_named:
                print_node(cursor.node, indent)
                mermaid.append(mermaid_statement(cursor.node, prev_node))
            prev_node = prev_named_node(cursor.node)
        else:
            if cursor.goto_next_sibling():
                recurse = True
                if cursor.node.is_named:
                    print_node(cursor.node, indent)
                    mermaid.append(mermaid_statement(cursor.node, prev_node))
            elif cursor.goto_parent():
                indent -= 2
                recurse = False
                prev_node = prev_named_node(cursor.node)
            else:
                done = True

    for line in mermaid:
        print(line)


if __name__ == '__main__':
    parser = Parser()
    parser.set_language(GO_LANGUAGE)

    tree = parser.parse(bytes(SOURCE, 'utf8'))

    walk(tree)
